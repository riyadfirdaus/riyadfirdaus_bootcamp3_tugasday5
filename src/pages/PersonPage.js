import React from "react";
import Header from "../components/Header";
import style from "./PersonPage.module.css";

function PersonPage({ location }) {
  const props = location.state;
  console.log(location);
  return (
    <div className={style.app}>
      <Header noButton={true} />
      <div className={style.window}>
        <h2>{props.name}</h2>
        <p>
          {props.name} beralamat di <strong>{props.address}</strong> dan
          mempunyai hobby <strong>{props.hobby}</strong>
        </p>
      </div>
    </div>
  );
}

export default PersonPage;
