import React from "react";
import Header from "../components/Header";
import style from "./Hobby.module.css";

function Hobby(props) {
  const userList = props.userList;
  return (
    <div className={style.app}>
      <Header noButton={true} />
      <div className={style.window}>
        <h1>Hobby</h1>
        <p>List Hobby:</p>
        <ul>
          {userList.map((user, index) => {
            return <li key={index}>{user.hobby}</li>;
          })}
        </ul>
      </div>
    </div>
  );
}

export default Hobby;
