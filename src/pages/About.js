import React from "react";
import Header from "../components/Header";
import style from "./About.module.css";

function About() {
  return (
    <div className={style.app}>
      <Header noButton={true} />
      <div className={style.window}>
        <h1>About</h1>

        <p>
          App ini dibuat oleh <strong>Riyad Firdaus</strong> untuk memenuhi
          tugas bootcamp Day 5 dari Coding.id
        </p>
      </div>
    </div>
  );
}

export default About;
