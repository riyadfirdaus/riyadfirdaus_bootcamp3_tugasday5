import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import React from "react";
import { NavLink } from "react-router-dom";

function Header(props) {
  if (props.noButton) {
    return (
      <div>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="sticky">
            <Toolbar sx={{ justifyContent: "flex-start" }}>
              <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                <Button
                  component={NavLink}
                  to="/"
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  My App
                </Button>
                <Button
                  component={NavLink}
                  to="/about"
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  About
                </Button>
                <Button
                  component={NavLink}
                  to={{
                    pathname: "/hobby",
                  }}
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  Hobby
                </Button>
              </Box>
            </Toolbar>
          </AppBar>
        </Box>
      </div>
    );
  }
  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="sticky">
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              <Button
                component={NavLink}
                to="/"
                sx={{ my: 2, color: "white", display: "block" }}
              >
                My App
              </Button>
              <Button
                component={NavLink}
                to="/about"
                sx={{ my: 2, color: "white", display: "block" }}
              >
                About
              </Button>
              <Button
                component={NavLink}
                to="/hobby"
                sx={{ my: 2, color: "white", display: "block" }}
              >
                Hobby
              </Button>
            </Box>

            <Button onClick={props.buttonFunction} color="inherit">
              Add User
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}

export default Header;
