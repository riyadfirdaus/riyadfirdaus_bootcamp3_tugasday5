import { Box, Button } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";
import style from "./UserItem.module.css";

function UserItem(props) {
  const path = `/person/${props.id}`;
  return (
    <Box className={style.container}>
      <div className={style.name}>{props.name}</div>
      <div className={style.address}>{props.address}</div>
      <div className={style.hobby}>{props.hobby}</div>
      <div className={style.btn}>
        <Button
          variant="contained"
          size="small"
          color="primary"
          component={NavLink}
          to={{
            pathname: path,
            state: {
              name: props.name,
              address: props.address,
              hobby: props.hobby,
            },
          }}
        >
          View
        </Button>
        <Button
          variant="contained"
          size="small"
          color="primary"
          onClick={props.function}
        >
          Edit
        </Button>
      </div>
    </Box>
  );
}

export default UserItem;
