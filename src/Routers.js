import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import App from "./App";
import About from "./pages/About";
import Hobby from "./pages/Hobby";
import PersonPage from "./pages/PersonPage";

function Routers() {
  const [userList, setUserList] = useState([
    { name: "Riyad Firdaus", address: "Cirebon", hobby: "Ngoding" },
    { name: "Andi Surandi", address: "Jakarta", hobby: "Membaca" },
    { name: "Budi Bakriyadi", address: "Surabaya", hobby: "Tidur" },
  ]);

  useEffect(() => {
    console.log(userList);
  }, [userList]);

  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/">
            <App userList={userList} setUserList={setUserList} />
          </Route>
          <Route exact path="/about">
            <About userList={userList} />
          </Route>
          <Route exact path="/hobby">
            <Hobby userList={userList} />
          </Route>
          <Route path="/person/:id" component={PersonPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default Routers;
